# MarIA la Biblioteca Nacional y el BSC crean un sistema especializado en el idioma español para mejorar las respuestas de las IA en nuestra lengua

[https://www.xataka.com/robotica-e-ia/maria-biblioteca-nacional-bsc-crean-sistema-especializado-idioma-espanol-para-mejorar-respuestas-ia-nuestra-lengua](https://www.xataka.com/robotica-e-ia/maria-biblioteca-nacional-bsc-crean-sistema-especializado-idioma-espanol-para-mejorar-respuestas-ia-nuestra-lengua)

[https://github.com/PlanTL-SANIDAD/lm-spanish](https://github.com/PlanTL-SANIDAD/lm-spanish)

[https://huggingface.co/BSC-TeMU/roberta-large-bne](https://huggingface.co/BSC-TeMU/roberta-large-bne)

```
$ python3 roberta.py "la inteligencia <mask> es el presente"
[' emocional', ' humana', ' no', ' artificial', ' colectiva']

$ python3 roberta.py "la curiosidad <mask> al gato"
[' mueve', ' mató', ' mata', ' ayuda', ' atrae']
```

## Build

```
docker-compose build

or

docker build -t maria ./maria
```

## Run

```
$ docker run --name maria --rm maria "hola <mask>, ¿qué tal?"
[' guapa', ' chicas', ' amigos', ' chicos', ' amigo']
```

## License

MIT
